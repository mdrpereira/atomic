import requests
import lxml.html as lh
import re
import sqlite3

#Url of desired webpage
url = "https://www.angelo.edu/faculty/kboudrea/periodic/structure_mass.htm"

#Web request using given url
page = requests.get(url)

#Uses "lxml.html" library to get a string representation of all html code.
doc = lh.fromstring(page.content)

tr_elements = doc.xpath('//tr')
    
res = []

for t in tr_elements[19:]:
    
    name = t.text_content()
    
    name = re.sub('\s+', ' ', name)
    
    name = re.sub('[()]','', name)
    
    res.append(name.strip().split(" "))
    
for i in res:
    print(i)

#==============================================================================
SQL_CREATE = """ CREATE TABLE Elements (
        Number INTEGER,
        Symbol CHAR(3),
        Name   TEXT,
        Weight REAL,
        PRIMARY KEY (Symbol)); """

SQL_INSERT = """ INSERT INTO Elements VALUES (
        {0}, "{1}", "{2}", {3})"""
    
def SQL():
    conn = sqlite3.connect("atomic.db")
    cur = conn.cursor()
    
    cur.execute(SQL_CREATE)
    
    conn.close()
    
def SQL_FILL():
    conn = sqlite3.connect("atomic.db")
    cur = conn.cursor()
    
    for i in res:
        cur.execute(SQL_INSERT.format(*i))
     
        conn.commit()
    
    conn.close()

def SQL_CLEAR():
    conn = sqlite3.connect("atomic.db")
    cur  = conn.cursor()
    
    cur.execute('DROP TABLE Elements;')
    
    conn.commit()
    
    conn.close()


if __name__ == '__main__':
    SQL_CLEAR()

    SQL()

    SQL_FILL()