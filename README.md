Simple TUI based molecular weight calculator.

Planned features:
- Automatic molecule detection using chemical databases.
- Molecule search using formulae in conjuction with previous feature.
- On-site molecule database according to users' search history.