import numpy as np
import sqlite3

#Hello There

SQL_QUERY_WEIGHT = """ SELECT Weight FROM Elements WHERE Symbol="{}" """ 

def atomic_weight(symbol, cur):

    cur.execute(SQL_QUERY_WEIGHT.format(symbol))

    res = cur.fetchone()

    return(res[0])

#=========================

def molecular_weight(inp):
    res = 0
     
    symbol = None
    amt = 1

    conn = sqlite3.connect("atomic.db")
    cur = conn.cursor()

    first = True

    for i in range(len(inp)):
        elem = inp[i]
        # If element is uppercase, then it is assumed that a new element is being stated, that leads to a "flush" of the buffer (amount 'amt' and symbol).
        if (elem.isupper()):

            # Weight of element is added to result
            if first != True:
                res += (atomic_weight(symbol, cur) * amt)
                amt = 1

            # If it is the first loop then the buffer is empty and would produce an error if attempted to calculate atomic weight
            else:
                first = False

            symbol = elem

        # If element is numeric, then amount 'amt' is set
        elif (elem.isnumeric()):
            if inp[i-1].isnumeric():
                amt = int(str(amt) + elem)
            else:
                amt = int(elem)

        elif (elem == "(") :
            a = i
            b = inp.index(")")

            res += molecular_weight(inp[a+1:b]) * float(inp[b+1])

            if  b+1 == (len(inp) - 1):
                return(res)

            del inp[a:b+1]



        # Else --> symbolizes a lowercase element, which indicates a continuation of the current symbol ( 'F' + 'e' = Fe (Iron)). 
        else:
            symbol += elem

        # Checks if the current element is the last specified, if so, then buffer is flushed and weight is added to the result
        if i == (len(inp) - 1):
            res += (atomic_weight(symbol, cur) * amt)

            return(res)

    conn.close()

    return(res)

#==============================================================================

if __name__ == "__main__":

    # Main loop
    while True:
        inp = input("Input: ")

        # Exit condition (requires user to input 'quit' or 'q')
        if (inp == "quit") or (inp == "q"):
            print("Goodbye")
            break

        # Transforms input string into a list for easier manipulation
        inp = list(inp)

        # If the first element is a number (representing multiple instances of a molecule), then the return value is multipllied by it.
        if inp[0].isnumeric():
            mult = inp[0]

            inp.pop(0)
            
            print(str(int(mult) * molecular_weight(inp)) + " g/mol")

        else:
            print(str(molecular_weight(inp)) + " g/mol")
